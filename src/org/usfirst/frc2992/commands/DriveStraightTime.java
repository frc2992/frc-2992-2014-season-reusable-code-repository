/**
 *
 * File DriveStraightTime.java -- Implements a command to drive the robot
 * straight forward (or backwards) for a specified time at a specified speed.
 *
 * Various constructors allow it to be used different ways.
 *
 */
package org.usfirst.frc2992.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc2992.Robot;
import org.usfirst.frc2992.subsystems.DriveTrain;

public class DriveStraightTime extends Command {

    // Local reference for convenience
    DriveTrain drivetrain;

    // How long we want to drive for
    double duration = 4.0;      // default time (seconds)

    // How fast to drive (- is reverse, range -1.0 to 1.0)
    double speed = 0.7;         // default speed

    // What gear to use
    boolean highGear = false;   // default gear

    // Reset gyro before starting?
    boolean resetGyro = true;
    
    // Battery voltage
    double batVolts;
    
    /**
     *
     * Constructor with no arguments. We drive for the default time, speed and
     * gear.
     *
     */
    public DriveStraightTime() {
        // Stop whatever else the drivetrain is doing
        requires(Robot.driveTrain);

        // No changes to default values
    }

    /**
     *
     * Constructor with time value specified. Uses default speed and gear
     *
     * @param howlong -- how long to drive (seconds)
     *
     */
    public DriveStraightTime(double howlong) {
        // Stop whatever else the drivetrain is doing first
        requires(Robot.driveTrain);

        // And set our time duration
        duration = howlong;
    }

    /**
     *
     * Constructor specifying how long and how fast to go. Uses default gear.
     *
     * @param howlong -- time to drive (seconds)
     * @param howfast -- motor speed to use (-1.0 reverse full to 1.0 full)
     *
     */
    public DriveStraightTime(double howlong, double howfast) {
        // Stop whatever else the drivetrain is doing first
        requires(Robot.driveTrain);

        // Set the time and speed
        duration = howlong;
        speed = howfast;
    }

    /**
     *
     * Constructor specifying how long and how fast to go and the gear.
     *
     * @param howlong -- time to drive (seconds)
     * @param howfast -- motor speed to use (-1.0 reverse full to 1.0 full)
     * @param high -- which gear. true=high gear, false=low
     *
     */
    public DriveStraightTime(double howlong, double howfast, boolean high) {
        // Stop whatever else the drivetrain is doing first
        requires(Robot.driveTrain);

        // Set the time and speed and gear
        duration = howlong;
        speed = howfast;
        highGear = high;
    }

        /**
     *
     * Constructor specifying how long and how fast to go and the gear.
     *
     * @param howlong -- time to drive (seconds)
     * @param howfast -- motor speed to use (-1.0 reverse full to 1.0 full)
     * @param high -- which gear. true=high gear, false=low
     * @param gyro -- gyro control.  true=reset at start, false=don't
     * 
     */
    public DriveStraightTime(double howlong, double howfast, boolean high, 
            boolean gyro) {
        
        // Stop whatever else the drivetrain is doing first
        requires(Robot.driveTrain);

        // Set the time and speed and gear
        duration = howlong;
        speed = howfast;
        highGear = high;
        resetGyro = gyro;
    }


    
    // Called just before this Command runs the first time
    protected void initialize() {
        
        // Local reference for convenience
        drivetrain = Robot.driveTrain;
        //batVolts = Robot.ds.getBatteryVoltage();
        //System.out.println("Voltage = " + batVolts);
        // Make sure the gyro is zeroed if needed
        if (resetGyro) {
            drivetrain.resetGyro();
            //System.out.println("Gyro reset");
        }

        // Set the gear
        if (highGear) {
            drivetrain.upShift();
        } else {
            drivetrain.downShift();
        }

        this.setTimeout(duration);   // Set timer on how long to run

    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        // Read the gyro
        double angle = drivetrain.getGyroAngle();
        
        // Scale adjustment on how aggresively to use gyro steering
        double gyroFactor = -0.15;
        
        // Scale speed by voltage
        // batVolts = Robot.ds.getBatteryVoltage();
        // speed = speed * (12.5/batVolts);
        
        // If going backwards we need to reverse the gyro factor
        if (speed < 0)
            gyroFactor *= -1.0;
        
        // Now steer with turn adjustment based on gyro
        // THis next line of code is WRONG.  Should use gyroFactor
        // instead of the oonstant -0.15 to account for reverse motion
        drivetrain.arcadeDrive(speed, -0.15 * angle);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        // When timer expires we are done
        return this.isTimedOut();
    }

    // Called once after isFinished returns true
    protected void end() {
        // When timer expires and we are done we should stop
        drivetrain.allStop();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        // Also make sure we stop if another command interrupts us
        drivetrain.allStop();
    }
}
