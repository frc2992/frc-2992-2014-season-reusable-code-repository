/**
 *
 * File DriveHigh.java -- Command to put the drivetrain into high gear! Let's
 * get moving!
 *
 *
 */
package org.usfirst.frc2992.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc2992.Robot;

public class DriveHigh extends Command {

    public DriveHigh() {
        // We don't need to stop other commands on the drivetrain to shift it.
        // Thus we have commented out the requires statement!
        // requires(Robot.driveTrain);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        Robot.driveTrain.upShift();
    }

// Called repeatedly when this Command is scheduled to run
    protected void execute() {
        // Only need to do it once
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        // And we are done right away
        return true;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
