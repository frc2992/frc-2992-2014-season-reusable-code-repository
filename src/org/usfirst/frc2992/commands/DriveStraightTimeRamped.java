/**
 *
 * File DriveStraightTime.java -- Implements a command to drive the robot
 * straight forward (or backwards) for a specified time at a specified speed.
 *
 * Various constructors allow it to be used different ways.
 *
 */
package org.usfirst.frc2992.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc2992.Robot;
import org.usfirst.frc2992.subsystems.DriveTrain;

public class DriveStraightTimeRamped extends Command {

    // Local reference for convenience
    DriveTrain drivetrain;

    // How long we want to drive for
    double duration = 4.0;      // default time (seconds)

    // How fast to drive (- is reverse, range -1.0 to 1.0)
    // We have a start speed and a finish speed that we ramp
    double startSpeed = 0.0;         // default start speed
    double endSpeed = 0.7;          // default end speed

    // What gear to use
    boolean highGear = false;   // default gear

    // Reset gyro before starting?
    boolean resetGyro = true;
    
    // Battery voltage
    double batVolts;
    
    // Stuff for computing speed for each cycle
    int cycleCount = 0;
    double lastSpeed;           // Keep track of last speed we commanded
    double speedIncr;           // How much to change speed each cycle
    
    /**
     *
     * Constructor with no arguments. We drive for the default time, speed and
     * gear.
     *
     */
    public DriveStraightTimeRamped() {
        // Stop whatever else the drivetrain is doing
        requires(Robot.driveTrain);

        // No changes to default values
    }

    /**
     *
     * Constructor with time value specified. Uses default speed and gear
     *
     * @param howlong -- how long to drive (seconds)
     *
     */
    public DriveStraightTimeRamped(double howlong) {
        // Stop whatever else the drivetrain is doing first
        requires(Robot.driveTrain);

        // And set our time duration
        duration = howlong;
    }

    /**
     *
     * Constructor specifying how long and how fast to go. Uses default gear.
     *
     * @param howlong -- time to drive (seconds)
     * @param start -- motor start speed to use (-1.0 reverse full to 1.0 full)
     * @param end -- motor end speed to ramp to
     *
     */
    public DriveStraightTimeRamped(double howlong, double start, double end) {
        // Stop whatever else the drivetrain is doing first
        requires(Robot.driveTrain);

        // Set the time and start and stop speeds
        duration = howlong;
        startSpeed = start;
        endSpeed = end;
    }

    /**
     *
     * Constructor specifying how long and how fast to go and the gear.
     *
     * @param howlong -- time to drive (seconds)
     * @param start -- motor speed to use (-1.0 reverse full to 1.0 full)
     * @param end -- motor speed to ramp to
     * @param high -- which gear. true=high gear, false=low
     *
     */
    public DriveStraightTimeRamped(double howlong, double start, 
            double end, boolean high) {
        // Stop whatever else the drivetrain is doing first
        requires(Robot.driveTrain);

        // Set the time and speed and gear
        duration = howlong;
        startSpeed = start;
        endSpeed = end;
        highGear = high;
    }

        /**
     *
     * Constructor specifying how long and how fast to go and the gear.
     *
     * @param howlong -- time to drive (seconds)
     * @param start -- motor speed to use (-1.0 reverse full to 1.0 full)
     * @param end -- motor speed to ramp to
     * @param high -- which gear. true=high gear, false=low
     * @param gyro -- gyro control.  true=reset at start, false=don't
     * 
     */
    public DriveStraightTimeRamped(double howlong, double start, double end,
            boolean high, boolean gyro) {
        // Stop whatever else the drivetrain is doing first
        requires(Robot.driveTrain);

        // Set the time and speed and gear
        duration = howlong;
        startSpeed = start;
        endSpeed = end;

        highGear = high;
        resetGyro = gyro;
    }


    
    // Called just before this Command runs the first time
    protected void initialize() {
        // Local reference for convenience
        drivetrain = Robot.driveTrain;
        //batVolts = Robot.ds.getBatteryVoltage();
        // System.out.println("Voltage = " + batVolts);
        
        // Make sure the gyro is zeroed if needed
        if (resetGyro)
            drivetrain.resetGyro();

        // Set the gear
        if (highGear) {
            drivetrain.upShift();
        } else {
            drivetrain.downShift();
        }

        
        // Do some calculations for our ramp 1 time
        cycleCount = 0;
        int numCycles = (int)(duration * 50.0);   // How many cycles we have
        lastSpeed = startSpeed;
        speedIncr = (endSpeed - startSpeed) / numCycles;
        
        // Kick it off and then we will ramp from there.  No need to steer first time
        drivetrain.arcadeDrive(startSpeed, 0.0);
        
        //System.out.println("Start speed = " + startSpeed);
        //System.out.println("End speed = " + endSpeed);
        //System.out.println("Duration = " + duration);
        //System.out.println("Speed Incr = " + speedIncr);
        //System.out.println("");
        this.setTimeout(duration);   // Set timer on how long to run

    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        // Read the gyro
        double angle = drivetrain.getGyroAngle();

        // Figure out next speed
        lastSpeed = lastSpeed + speedIncr;
        if ((lastSpeed < -1.05) || (lastSpeed > 1.05)) {
            // We screwed up -- should never get here
            lastSpeed = 0.0;        // Stop!
            speedIncr = 0.0;        // And keep it that way
            //System.out.println("Ramp calculation failed");
        }
        
        //System.out.println("Speed: " + lastSpeed);
        double speed = lastSpeed;
        
        // Scale adjustment on how aggresively to use gyro steering
        double gyroFactor = -0.15;
        
        // Scale speed by voltage
        // batVolts = Robot.ds.getBatteryVoltage();
        // speed = speed * (12.5/batVolts);
        
        // If going backwards we need to reverse the gyro factor
        if (speed < 0.0)
            gyroFactor *= -1.0;
        
        // Now steer with turn adjustment based on gyro
        drivetrain.arcadeDrive(speed, gyroFactor * angle);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        // When timer expires we are done
        return this.isTimedOut();
    }

    // Called once after isFinished returns true
    protected void end() {
        // When timer expires and we are done we should stop
        drivetrain.allStop();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        // Also make sure we stop if another command interrupts us
        drivetrain.allStop();
    }
}
