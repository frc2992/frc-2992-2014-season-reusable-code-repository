/**
 *
 * File CompressorOn.java -- Command to turn compressor on.
 *
 */
package org.usfirst.frc2992.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc2992.Robot;

/**
 *
 */
public class CompressorOff extends Command {

    public CompressorOff() {
        // Stops all other commands on subsystem before running
        requires(Robot.compressor);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        // Stop pumping air
        Robot.compressor.stop();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        // Only need to do it once
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        // Stays this way until another command force changes it
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
