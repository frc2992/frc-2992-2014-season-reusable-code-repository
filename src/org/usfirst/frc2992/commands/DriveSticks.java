/**
 *
 * File DriveSticks.java -- This is the command that we run while doing operator
 * controlled (joystick) driving of the robot. It is often the default command,
 * so we revert to joystick driving if we are doing nothing else.
 *
 *
 */
package org.usfirst.frc2992.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc2992.Robot;
import org.usfirst.frc2992.subsystems.DriveTrain;
import org.usfirst.frc2992.mhJoystick;

public class DriveSticks extends Command {

    // Local variables to reference the subsystem and joysticks for convenience
    mhJoystick myStick;
    DriveTrain drivetrain;

    public DriveSticks() {
        // Stop all other commands on the drivetrain before we start this
        requires(Robot.driveTrain);

    }

    // Called just before this Command runs the first time
    protected void initialize() {
        // Get local references
        myStick = Robot.oi.myStick;
        drivetrain = Robot.driveTrain;
        // Make sure its not still doing something else
        drivetrain.allStop();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {

        // First force a check on which sticks to use
        myStick.checkTypeSwitch();

        // Check gearshift
        if (myStick.upShift()) {
            drivetrain.upShift();
        } else if (myStick.downShift()) {
            drivetrain.downShift();
        }

        // Check for tank or arcade mode selector pressed
        if (myStick.arcadeMode()) {
            drivetrain.setTankMode(false);
        }
        if (myStick.tankMode()) {
            drivetrain.setTankMode(true);
        }

        // Read stick values
        double leftYval = myStick.leftY();
        double rightYval = myStick.rightY();
        double rightXval = myStick.rightX();

        // Now drive in either tank or arcade as mode says
        if (drivetrain.isTankMode) {
            drivetrain.tankDrive(leftYval, rightYval);
        } else {
            drivetrain.arcadeDrive(leftYval, rightXval);
        }

    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        // Runs until it is forced to stop by some other command
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
