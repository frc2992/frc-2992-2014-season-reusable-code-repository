/**
 *
 * File CompressorOn.java -- Command to turn compressor on.
 *
 */
package org.usfirst.frc2992.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc2992.Robot;

public class ReadKinect extends Command {

    public int numHotSeen = 0;
    public int loopCount = 0;
    public static final int firstRead = 25; // Start at 0.5 second
    public static final int lastRead = 125; // Stop at 2.5 seconds
    public static final int howOften = 5;   // Read every 5th cycle
    public static final int numNeeded = 7;   // How many hot b4 we believe
    public static final double hotThreshold = -0.5;   // Hot is -1.0 to this
    
    public ReadKinect() {
        // Stops all other commands on subsystem before running
        // No subsystem required
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        //System.out.println("ReadKinect started at " + Robot.ds.getMatchTime());
        numHotSeen = 0;
        loopCount = 0;
        
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        // Increment counter
        loopCount++;
        
        if (loopCount >= firstRead && loopCount <= lastRead) {
            // We are in the interval we want to read
            if (loopCount % howOften == 0) {
                if (Robot.oi.readKinect() < hotThreshold) {
                    // We have a hot reading so count it
                    numHotSeen++;
                } 
            }
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        // Runs until count greater than our stop point
        return (loopCount > lastRead);
    }

    // Called once after isFinished returns true
    protected void end() {
        // Set the robot flag indicating whether its hot or not
        //System.out.println("ReadKinect done at " + Robot.ds.getMatchTime());
        Robot.goalHot = (numHotSeen >= numNeeded);
        Robot.oi.setHotGoalLights(true, Robot.goalHot);
        //System.out.println("Number of hot samples: " + numHotSeen);
        //System.out.println("HotZOne detected:  " + Robot.goalHot);
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
