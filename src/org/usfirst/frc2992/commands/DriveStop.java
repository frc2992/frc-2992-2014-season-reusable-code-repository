/**
 *
 * File DriveStop.java -- Command to stop the drivetrain right now.
 *
 *
 */
package org.usfirst.frc2992.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc2992.Robot;
import org.usfirst.frc2992.subsystems.DriveTrain;

/**
 *
 */
public class DriveStop extends Command {

    public DriveStop() {
        // Force all other drivetrain commands to stop
        requires(Robot.driveTrain);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        // Stop it right now
        Robot.driveTrain.allStop();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        // Only need to do it once
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        // We are done with command right away
        return true;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
