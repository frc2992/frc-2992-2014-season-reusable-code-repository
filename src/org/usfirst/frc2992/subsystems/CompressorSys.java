/**
 *
 * File CompresserSys.java -- Subsystem for pneumatic compressor
 *
 * This is a pretty trivial subsystem which includes a compressor and its
 * pressure switch. The library automatically cycles the compressor as needed
 * based on pressure switch reading.
 *
 */
package org.usfirst.frc2992.subsystems;

import org.usfirst.frc2992.commands.CompressorOn;
import org.usfirst.frc2992.RobotMap;
import edu.wpi.first.wpilibj.*;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 */
public class CompressorSys extends Subsystem {

    // Get a local reference for convenience
    Compressor compress = RobotMap.compressorPressureCompressor;

    /**
     *
     * Set the default command to turn compressor on. No reason why we ever want
     * it off when robot is enabled.
     *
     */
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.  Always start compressor
        setDefaultCommand(new CompressorOn());
    }

    /**
     *
     * Method to start the compressor.
     *
     */
    public void start() {
        compress.start();
    }

    /**
     *
     * Method to stop the compressor. We want it always running if enabled so
     * probably only used in disabled state.
     *
     */
    public void stop() {
        compress.stop();
    }
    public boolean isRunning() {
        return compress.enabled();
    }
}
