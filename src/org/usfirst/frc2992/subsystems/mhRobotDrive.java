/**
 *
 * mhRobotDrive.java -- The purpose of this class is to create a custom robot
 * drive system object for team 2992. It supports an arbitrary number of motors
 * on each side, with the ability to reverse the motors direction setting if
 * needed.
 *
 */
package org.usfirst.frc2992.subsystems;

import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc2992.Robot;

public class mhRobotDrive {

    // Array of drive motor controllers for left and right
    // We use an enhanced Victor object that is "reversible"
    reversibleVictor[] leftDriveMotors;
    reversibleVictor[] rightDriveMotors;

    // Square joystick inputs to make driving smoother?
    boolean squareStickInputs = true;

    /**
     *
     * Constructor method for drive chassis. Takes in 2 array lists of motor
     * cannel numbers and creates the motor objects.
     *
     * @param leftMotorChannels -- int[] array of left motor channel numbers
     * @param rightMotorChannels -- int[] array of right motor channel numbers
     */
    public mhRobotDrive(int[] leftMotorChannels, int[] rightMotorChannels) {

        // Create our arrays of motor controller objects
        leftDriveMotors = new reversibleVictor[leftMotorChannels.length];
        rightDriveMotors = new reversibleVictor[rightMotorChannels.length];

        // And instantiate the controllers.  We default to left motors are
        // reversed and right motors are not reversed.
        for (int i = 0; i < leftMotorChannels.length; i++) {
            leftDriveMotors[i] = new reversibleVictor(leftMotorChannels[i], true);
            LiveWindow.addActuator("DriveTrain", "Left Motor "+ i, leftDriveMotors[i].getVictorRef());
        }
        for (int i = 0; i < rightMotorChannels.length; i++) {
            rightDriveMotors[i] = new reversibleVictor(rightMotorChannels[i], false);
            LiveWindow.addActuator("DriveTrain", "Right Motor " + i, rightDriveMotors[i].getVictorRef());
        }
    }

    /**
     *
     * Method to turn motor safety feature on/off on the chassis. If safety is
     * turned on, you must update the speed setting on the motor within the
     * safety interval or motor will shut down.
     *
     * @param safetyOnOff -- true = safety features on, false = off
     *
     */
    public void setSafety(boolean safetyOnOff) {
        for (int i = 0; i < leftDriveMotors.length; i++) {
            leftDriveMotors[i].motor.setSafetyEnabled(safetyOnOff);
        }
        for (int i = 0; i < rightDriveMotors.length; i++) {
            rightDriveMotors[i].motor.setSafetyEnabled(safetyOnOff);
        }
    }

    /**
     *
     * Tank drive method. Takes in left and right tank drive joystick input
     * values and drives the left and right motors. Input values should be -1.0
     * (reverse) to 1.0 (forward).
     *
     * @param leftJoystickValue
     * @param rightJoystickValue
     */
    public void tankDrive(double leftJoystickValue, double rightJoystickValue) {

        double leftspeed = leftJoystickValue;
        double rightspeed = rightJoystickValue;

        // Square the stick inputs to make less sensitive near 0 point
        if (squareStickInputs) {
            leftspeed = smoothPowerCurve(leftJoystickValue);
            rightspeed = smoothPowerCurve(rightJoystickValue);
        }

        // Apply motor speed ramp
        // leftspeed = rampMotorSpeed(leftspeed, 
        //            getDriveSpeed(leftDriveMotors),0.1);
        // rightspeed = rampMotorSpeed (rightspeed, 
        //            getDriveSpeed(rightDriveMotors),0.1);
        // Set the drive motors
        setDriveSpeed(leftDriveMotors, leftspeed);
        setDriveSpeed(rightDriveMotors, rightspeed);

        // Smartdashboard update
        // SmartDashboard.putNumber("Left Motor Speed", leftspeed);
        // SmartDashboard.putNumber("Right Motor Speed", rightspeed);
    }

    /**
     *
     * Arcade (FPS) drive method. Takes in a throttle value and a turn value and
     * performs arcade driving on the robot.
     *
     * @param moveValue -- throttle. -1.0 (reverse) to 1.0 (forward)
     * @param turnValue -- turn amount -1.0 (hard left) to 1.0 (hard right)
     *
     */
    public void arcadeDrive(double moveValue, double turnValue) {

        double leftVal;
        double rightVal;

        // Square the inputs to make less sensitive near 0 point
        if (squareStickInputs) {
            moveValue = smoothPowerCurve(moveValue);
            turnValue = smoothPowerCurve(turnValue);
        }

        // SmartDashboard.putNumber("Squared left value", moveValue);
        // SmartDashboard.putNumber("Squared right value", turnValue);
        // Figure out the left and right motor values
        // Basic process is to slow wheels on one side by the turn value
        if (moveValue >= 0.0) {
            // Going forward
            if (turnValue >= 0.0) {
                // Forward right turn
                // Left motor is the throttle, or if too slow the turn value
                leftVal = Math.max(moveValue, turnValue);
                // Right motor is throttle minus the turn to slow it down
                rightVal = moveValue - turnValue;
            } else {
                // Forward left turn
                // Left motor is throttle minus the turn to slow it down
                leftVal = moveValue + turnValue;    // turnValue is negative  
                // Right motor is throttle, or if too slow the turn value
                rightVal = Math.max(moveValue, -turnValue); // turnValue is negative
            }
        } else {
            // Going backwards
            if (turnValue >= 0.0) {
                // Backward right turn
                // Left motor is the throttle, or turn value if too slow
                leftVal = -Math.max(-moveValue, turnValue); // move is negative
                // Right motor is throttle minus turn
                // move is negative so add the positive turn
                rightVal = moveValue + turnValue;
            } else {
                // Backward left turn
                // Left motor is throttle minus turn
                // Both are negative so we just subtract
                leftVal = moveValue - turnValue;
                // Right motor is move, or if too slow the turn value
                // Both are negative
                rightVal = -Math.max(-moveValue, -turnValue);
            }
        }

        // Apply motor speed ramp
        // leftVal = rampMotorSpeed(leftVal, )
        //            getDriveSpeed(leftDriveMotors),0.1);
        // rightVal = rampMotorSpeed (rightVal, )
        ////            getDriveSpeed(rightDriveMotors),0.1);
        setDriveSpeed(leftDriveMotors, leftVal);
        setDriveSpeed(rightDriveMotors, rightVal);

        // Smartdashboard update
        // SmartDashboard.putNumber("Left Motor Speed", leftVal);
        // SmartDashboard.putNumber("Right Motor Speed", rightVal);
    }

    /**
     *
     * Method to set the direction of left side motors
     *
     * @param reverseIt -- true = reverse the motors
     */
    public void reverseLeftMotors(boolean reverseIt) {
        for (int i = 0; i < leftDriveMotors.length; i++) {
            leftDriveMotors[i].setReverse(reverseIt);
        }
    }

    /**
     *
     * Method to set the direction of a specific left side motor
     *
     * @param whichMotor -- motor number to set
     * @param reverseIt -- true = reverse the motor
     */
    public void reverseLeftMotors(int whichMotor, boolean reverseIt) {
        // Validate its a valid motor number
        if ((whichMotor >= 0) && (whichMotor < leftDriveMotors.length)) {
            leftDriveMotors[whichMotor].setReverse(reverseIt);
        }
    }

    /**
     *
     * Method to set the direction of right side motors
     *
     * @param reverseIt -- true = reverse the motors
     */
    public void reverseRightMotors(boolean reverseIt) {
        for (int i = 0; i < rightDriveMotors.length; i++) {
            rightDriveMotors[i].setReverse(reverseIt);
        }
    }

    /**
     *
     * Method to set the direction of a specific right side motor
     *
     * @param whichMotor -- motor number to set
     * @param reverseIt -- true = reverse the motor
     *
     */
    public void reverseRightMotors(int whichMotor, boolean reverseIt) {
        // Validate its a valid motor number
        if ((whichMotor >= 0) && (whichMotor < rightDriveMotors.length)) {
            rightDriveMotors[whichMotor].setReverse(reverseIt);
        }
    }

    /**
     *
     * Method to set whether we are using squared joystick inputs
     *
     * @param square -- True = square all inputs before using
     *
     */
    public void squareJoystickInputs(boolean square) {
        squareStickInputs = square;
    }

    /**
     *
     * Method to retrieve the current number of motors on the left side
     *
     * @return -- int number of left motors
     */
    public int howManyLeftMotors() {
        return leftDriveMotors.length;
    }

    /**
     *
     * Method to retrieve the current number of motors on the right side
     *
     * @return -- int number of right motors
     */
    public int howManyRightMotors() {
        return rightDriveMotors.length;
    }

    /**
     *
     * Internal utility method to actually set the drive motors directly.
     * Private so should not be used outside this class file.
     *
     * @param motors -- left or ride side array of motors to set
     * @param speed -- speed to set the motors to
     *
     */
    private void setDriveSpeed(reversibleVictor[] motors, double speed) {
        // Set for all motors in the array
        for (int i = 0; i < motors.length; i++) {
            if (motors[i].getReverse()) // The motor is reversed
            {
                motors[i].motor.set(-speed);
            } else // The motor is not reversed
            {
                motors[i].motor.set(speed);
            }
        }
    }

    /**
     *
     * Internal utility method to read the current speed of a set of motors.
     * Really just reads the first motor and assumes they are all the same
     * speed.
     *
     * @param motors -- array of left or right motors
     *
     * @return -- the current speed (accounting for whether reversed)
     *
     */
    private double getDriveSpeed(reversibleVictor[] motors) {
        double speed = 0.0;

        // Make sure we have a motor before we read it
        if (motors.length > 0) {
            speed = motors[0].motor.get();
            if (motors[0].getReverse()) // The motor is reversed
            {
                speed *= -1.0;
            }
        }
        return speed;
    }

    /**
     * Internal private utility method computes the square of a value but
     * preserves the sign.
     *
     * @param x -- double
     *
     * @return -- x squared but sign preserved
     *
     */
    //private double squaredWithSign(double x) {
    //    if (x < 0) {
    //        return (-1 * x * x);
    //    } else {
    //        return (x * x);
    //    }
    //}

     /**
     * This method smooths out the motor values using cubic equation 
     * and preserves the sign.
     * 
     * @param x
     * @return 
     */
    private double smoothPowerCurve (double x) {
        //double a = Robot.oi.myStick.getThrottle();
        double a = 1.0;         // Hard code to max 
        double b = 0.15;
        
        if (x > 0.0)
            return (b + (1.0-b)*(a*x*x*x+(1.0-a)*x));
        
        else if (x<0.0)
            return (-b + (1.0-b)*(a*x*x*x+(1.0-a)*x));
        else return 0.0;
    }


    /**
     * Internal private utility method to ramp/smooth motor speeds. Not
     * currently used, but can be applied to any drive method if it is required
     * to "smooth" the driving experience. The basic idea is to restrict how
     * much a motors speed can be changed in one cycle to a hard limit. All
     * speeds should be in range -1.0 to 1.0.
     *
     * @param requestedSpeed -- what speed we want to get to
     * @param presentSpeed -- what speed the motor is currently turning
     * @param modifiedSpeed -- how much we allow it to change in one cycle
     *
     * @return -- the actual motor speed to use
     *
     */
    private double rampMotorSpeed(double requestedSpeed, double presentSpeed,
            double modifiedSpeed) {
        double newMotorSpeed = 0.0;

        // comparing the desired speed to the current speed,  
        //   if they are the same it moves on, if not it goes to the else 
        if (requestedSpeed == presentSpeed) {
            newMotorSpeed = requestedSpeed;
        } else {
            if (requestedSpeed < presentSpeed) {
                // if the requested speed is less then the current speed  
                //    it will subtract the desired speed from the current speed  
                //    to get the interval to change the speed 
                if ((presentSpeed - requestedSpeed) < modifiedSpeed) {
                    // Speed change is within the bounds we allow
                    newMotorSpeed = requestedSpeed;
                } else {
                    // Speed change is too high -- only change by the limit
                    newMotorSpeed = presentSpeed - modifiedSpeed;
                }
            } else if (requestedSpeed > presentSpeed) {
                // if the requested speed is greater then the current speed  
                //    it will subtract the current speed from the desired speed  
                //    to get the interval to change the speed                    
                if ((requestedSpeed - presentSpeed) < modifiedSpeed) {
                    // Speed change is within bounds we allow
                    newMotorSpeed = requestedSpeed;
                } else {
                    // Speed change is too high -- only change by the limit
                    newMotorSpeed = presentSpeed + modifiedSpeed;
                }
            }
        }
    

        return (newMotorSpeed);
    }
        public double getLeftSpeed(){
            return getDriveSpeed(leftDriveMotors);
        }
        public double getRightSpeed(){
            return getDriveSpeed(rightDriveMotors);
        }

}
