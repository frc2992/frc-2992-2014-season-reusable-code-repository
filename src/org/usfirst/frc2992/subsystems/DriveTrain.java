/**
 *
 * File DriveTrain.java -- Subsystem for robot drivetrain subsystem
 *
 * This subsystem represents the driving/steering system for the robot. It
 * includes not only the drive motors but also gyro and distance sensors which
 * may be used by drive commands to control steering distance and direction.
 *
 */
package org.usfirst.frc2992.subsystems;

import org.usfirst.frc2992.commands.DriveSticks;
import org.usfirst.frc2992.RobotMap;
import edu.wpi.first.wpilibj.*;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class DriveTrain extends Subsystem {

    // Get local references to gyro, chassis, and gearshift for convenience
    Gyro gyro = RobotMap.driveTrainGyro;
    Solenoid gearCylinder = RobotMap.driveTrainGearCylinder;
    AnalogChannel distSensor = RobotMap.distanceSensor;
    Relay lightShow = RobotMap.lightShow;
    public mhRobotDrive chassis = RobotMap.chassis;

    // Keep track of whether we are in tank or arcade mode.
    public boolean isTankMode = true;  // Default to tank mode

    // Constructor -- only thing to do is get the gyro cranked up    
    public DriveTrain() {
        gyro.reset();
    }

    // Sets which command to run if nothing else going on
    public void initDefaultCommand() {
        // If we have no other driving activity, let the sticks control it
        setDefaultCommand(new DriveSticks());
    }

    /**
     * Implements the tank drive capabilities of the drivetrain.
     *
     * @param left The speed for the left side of the drivetrain.
     * @param right The speed for the right side of the drivetrain.
     */
    public void tankDrive(double left, double right) {
        chassis.tankDrive(left, right);

        if (inRange()) {
            lightShowOn();

        } else {
            lightShowOff();
        }
    }

    /**
     *
     * Implements the arcade drive capabilities of the drivetrain.
     *
     * @param left -- the throttle value
     * @param right -- the turn value (-1.0 hard left to 1.0 hard right)
     */
    public void arcadeDrive(double left, double right) {
        chassis.arcadeDrive(left, right);
        if (inRange()) {
            lightShowOn();

        } else {
            lightShowOff();
        }
    }

    /**
     *
     * Stop this sucker now!
     *
     */
    public void allStop() {
        chassis.tankDrive(0.0, 0.0);
    }

    /**
     *
     * Force set it to tank or arcade mode
     *
     * @param isTank -- true=tank, false=arcade
     *
     */
    public void setTankMode(boolean isTank) {
        isTankMode = isTank;
        //if (isTankMode) {
            // SmartDashboard.putString("Drive Mode", "Tank");
        //} else {
            // SmartDashboard.putString("Drive Mode", "Arcade");
        //}
    }

    /**
     *
     * Method to get a string describing what drive mode we are in. Likely used
     * for building a driver display.
     *
     * @return String -- describes briefly what drive mode we are in.
     */
    public String getDriveModeString() {
        if (isTankMode) {
            return "Tank   ";
        } else {
            return "Arcade ";
        }
    }

    /**
     *
     * Put this sucker into high gear. I feel the need, the need for speed.
     *
     */
    public void upShift() {
        gearCylinder.set(false);
        // SmartDashboard.putString("Transmission", "High Gear");
    }

    /**
     *
     * Put this sucker into low gear. Pushing our weight around?
     *
     */
    public void downShift() {
        gearCylinder.set(true);
        // SmartDashboard.putString("Transmission", "Low Gear");
    }

    /**
     *
     * Read the gyro value (degrees we have turned since last reset
     *
     * @return -- double degrees (not limited to +/- 360!
     */
    public double getGyroAngle() {
        return gyro.getAngle();
    }

    /**
     *
     * Reset the gyroscope so current heading is 0 degrees.
     *
     */
    public void resetGyro() {
        gyro.reset();
    }

    public void lightShowOn() {
        lightShow.set(Relay.Value.kForward);

    }

    public void lightShowOff() {
        lightShow.set(Relay.Value.kOff);
    }

    public double getDistance() {
        return distSensor.getAverageValue();
    }

    public boolean inRange() {
        double distance = getDistance();
        boolean value = false;
        return (distance < 0.9) && (distance > 0.8);
 }
    public boolean inHighGear() {
        return !gearCylinder.get();
    }
}