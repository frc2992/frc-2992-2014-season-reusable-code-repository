/**
 *
 * File reversibleVictor.java -- Class which extends a Victor to add a flag as
 * to whether the motor direction needs to be reversed.
 *
 */
package org.usfirst.frc2992.subsystems;

import edu.wpi.first.wpilibj.Victor;

public class reversibleVictor  {

    // Flag indicating whether this motor is reversed.  true = reversed
    boolean reverse;
    Victor motor;

    /**
     *
     * Simple constructor with a Victor channel number. Motor will not be
     * reversed.
     *
     * @param channel
     *
     */
    public reversibleVictor(int channel) {
        // Call the Victor constructor to create Victor object
        motor = new Victor(channel);
        reverse = false;
    }

    /**
     *
     * Constructor with a Victor channel number. Motor will be reversed or not
     * based on boolean parameter
     *
     * @param channel
     * @param reverseIt -- true = motor reversed
     *
     */
    public reversibleVictor(int channel, boolean reverseIt) {
        // Call the Victor constructor to create Victor object
        motor = new Victor(channel);
        reverse = reverseIt;
    }

    /**
     *
     * Method to set a motor's reversed flag
     *
     * @param reverseIt -- true = reversed
     *
     */
    public void setReverse(boolean reverseIt) {
        reverse = reverseIt;
    }

    /**
     *
     * Method to retrieve a motor's reversed flag
     *
     * @return -- true = motor reversed
     *
     */
    public boolean getReverse() {
        return reverse;
    }
    
    public Victor getVictorRef(){
        return motor;
    }
}
