/**
 *
 * mhJoystick.java -- The purpose of this class is to create a "generic" custom
 * joystick object that allows the operator to pick which type of sticks they
 * want to use and makes it transparent to the rest of the code which type of
 * sticks are in use.
 *
 */
package org.usfirst.frc2992;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class mhJoystick {

    /**
     *
     * This subclass is to create an enum type to ID different types of
     * joysticks
     *
     */
    public static class Type {

        // Integer value representing enumeration
        public int value;

        static final int STICKS = 1;        // Tradition joystick
        static final int XBOX = 2;          // Xbox controller
        static final int NONE = 3;          // No stick

        public static final Type Sticks = new Type(STICKS);
        public static final Type Xbox = new Type(XBOX);
        public static final Type None = new Type(NONE);

        // Constructor used to create the enum Types
        private Type(int typeVal) {
            this.value = typeVal;
        }
    }

    // per instance class variables
    Type stickType;                 // Which sticks are currently active
    Joystick leftStick;
    Joystick rightStick;
    Joystick xbox;

    // Zero bias values -- Updated if we "zero" the sticks
    double leftXBias = 0.0;
    double leftYBias = 0.0;
    double leftYawBias = 0.0;
    double rightXBias = 0.0;
    double rightYBias = 0.0;
    double rightYawBias = 0.0;
    double xLeftXBias = 0.0;
    double xLeftYBias = 0.0;
    double xRightXBias = 0.0;
    double xRightYBias = 0.0;
    double xYawBias = 0.0;

    /**
     *
     * mhJoystick contructor based on channel numbers. Creates joystick objects
     * for 2 "normal" joysticks and 1 xbox type controller. Sets the type of
     * sticks currently active in this priority order: Normal sticks, xbox, or
     * none if no hardware is used.
     *
     * @param leftChan -- USB channel number of left stick
     * @param rightChan -- USB channel number of right stick
     * @param xboxChan -- USB channel number of xbox controller
     */
    public mhJoystick(int leftChan, int rightChan, int xboxChan) {

        // Create the joystick objects
        if (leftChan != 0) {
            leftStick = new Joystick(leftChan);
        }
        if (rightChan != 0) {
            rightStick = new Joystick(rightChan);
        }
        if (xboxChan != 0) {
            xbox = new Joystick(xboxChan);
        }

        if ((leftStick != null) && (rightStick != null)) {
            // Default to sticks if they are present
            stickType = Type.Sticks;
        } else if (xbox != null) {
            // Default to xbox if present and no regular sticks
            stickType = Type.Xbox;
        } else {
            stickType = Type.None;     // No sticks present at all!
        }

    }

    /**
     *
     * mhJoystick contructor based on Joystick objects. References joystick
     * objects for 2 "normal" joysticks and 1 xbox type controller. Sets the
     * type of sticks currently active in this priority order: Normal sticks,
     * xbox, or none if no hardware is used.
     *
     * @param theleft -- Joystick object to use for left stick
     * @param theright -- Joystick object to use for right stick
     * @param thexbox -- Joystick object to use for xbox controller
     */
    public mhJoystick(Joystick theleft, Joystick theright, Joystick thexbox) {
        leftStick = theleft;
        rightStick = theright;
        xbox = thexbox;

        if ((leftStick != null) && (rightStick != null)) {
            // Default to sticks if they are present
            stickType = Type.Sticks;
        } else if (xbox != null) {
            // Default to xbox if present and no sticks
            stickType = Type.Xbox;
        } else {
            stickType = Type.None;     // No sticks present at all!
        }
    }

    /**
     *
     * Method to retrieve the current joystick type in use.
     *
     * @return -- mhJoystick.Type of the sticks currently in use
     *
     */
    public Type getStickType() {
        return stickType;
    }

    /**
     *
     * Method to retrieve a short text string indicating the joysticks currently
     * in use. Normally used to create a driver display.
     *
     * @return -- String indicating the currently active joysticks
     *
     */
    public String getTypeString() {
        switch (stickType.value) {
            case mhJoystick.Type.STICKS:
                return "Jstk ";
            case mhJoystick.Type.XBOX:
                return "Xbox ";
            case mhJoystick.Type.NONE:
                return "None ";
        }
        return "   ";
    }

    /**
     *
     * Method to retrieve the left side X axis. Applies dead zone processing
     * which also clamps value to -1.0 to 1.0; Automatically uses the current
     * active sticks.
     *
     * @return -- double axis value
     *
     */
    public double leftX() {

        double value = 0.0;

        // Read and return the value
        try {
            switch (stickType.value) {
                case Type.STICKS:
                    value = dz(leftStick.getX() - leftXBias);
                    break;
                case Type.XBOX:
                    value = dz(xbox.getRawAxis(1) - xLeftXBias);
                    break;
                case Type.NONE:
                    value = 0.0;
                    break;
            }
        } catch (Exception e) {
            // Something failed reading sticks
            System.out.println("Read fail on controllers:  " + e.getMessage());
        }

        // SmartDashboard.putNumber("Stick LeftX", value);
        return value;
    }

    /**
     *
     * Method to retrieve the right side X axis. Applies dead zone processing
     * which also clamps value to -1.0 to 1.0; Automatically uses the current
     * active sticks.
     *
     * @return -- double axis value
     *
     */
    public double rightX() {

        double value = 0.0;

        // Read and return the value
        try {
            switch (stickType.value) {
                case Type.STICKS:
                    value = dz(rightStick.getX() - rightXBias);
                    break;
                case Type.XBOX:
                    value = dz(xbox.getRawAxis(4) - xRightXBias);
                    break;
                case Type.NONE:
                    value = 0.0;
                    break;
            }
        } catch (Exception e) {
            // Something failed reading sticks
            System.out.println("Read fail on controllers:  " + e.getMessage());
        }

        // SmartDashboard.putNumber("Stick RightX", value);
        return value;
    }

    /**
     *
     * Method to retrieve the Z or yaw axis. When used with 2 joysticks, it will
     * read the right hand stick yaw. Applies dead zone processing which also
     * clamps value to -1.0 to 1.0; Automatically uses the current active
     * sticks.
     *
     * @return -- double axis value
     *
     */
    public double yaw() {

        double value = 0.0;

        // Read and return the value
        try {
            switch (stickType.value) {
                case Type.STICKS:
                    value = dz(rightStick.getTwist() - rightYawBias);
                    break;
                case Type.XBOX:
                    // XBox right/left triggers are +- reversed
                    value = -1.0 * dz(xbox.getRawAxis(3) - xYawBias);
                    break;
                case Type.NONE:
                    value = 0.0;
                    break;
            }
        } catch (Exception e) {
            // Something failed reading sticks
            System.out.println("Read fail on controllers:  " + e.getMessage());
        }

        // SmartDashboard.putNumber("Stick Yaw", value);
        return value;
    }

    /**
     *
     * Method to retrieve the left side Y axis. Applies dead zone processing
     * which also clamps value to -1.0 to 1.0; Automatically uses the current
     * active sticks.
     *
     * @return -- double axis value
     *
     */
    public double leftY() {

        double value = 0.0;

        // Read and return the value. Negative is "forward"
        try {
            switch (stickType.value) {
                case Type.STICKS:
                    value = -1.0 * dz(leftStick.getY() - leftYBias);
                    break;
                case Type.XBOX:
                    value = -1.0 * dz(xbox.getRawAxis(2) - xLeftYBias);
                    break;
                case Type.NONE:
                    value = 0.0;
                    break;
            }
        } catch (Exception e) {
            // Something failed reading sticks
            System.out.println("Read fail on controllers:  " + e.getMessage());
        }

        // SmartDashboard.putNumber("Stick LeftY", value);
        return value;
    }

    /**
     *
     * Method to retrieve the right side Y axis. Applies dead zone processing
     * which also clamps value to -1.0 to 1.0; Automatically uses the current
     * active sticks.
     *
     * @return -- double axis value
     *
     */
    public double rightY() {

        double value = 0.0;

        // Read and return the value. Negative is "forward"
        try {
            switch (stickType.value) {
                case Type.STICKS:
                    value = -1.0 * dz(rightStick.getY() - rightYBias);
                    break;
                case Type.XBOX:
                    value = -1.0 * dz(xbox.getRawAxis(5) - xRightYBias);
                    break;
                case Type.NONE:
                    value = 0.0;
                    break;
            }
        } catch (Exception e) {
            // Something failed reading sticks
            System.out.println("Read fail on controllers:  " + e.getMessage());
        }

        // SmartDashboard.putNumber("Stick RightY", value);
        return value;
    }

    /**
     *
     * Method to retrieve the state of transmission upshift button. Reads from
     * current active sticks. If using dual joysticks, reads from the right
     * side.
     *
     * @return -- boolean -- true if the button is pressed
     *
     */
    public boolean upShift() {
        // Read and return the value.
        try {
            switch (stickType.value) {
                case Type.STICKS:
                    return rightStick.getRawButton(6);
                case Type.XBOX:
                    return xbox.getRawButton(6);
                case Type.NONE:
                    return false;
            }
        } catch (Exception e) {
            // Something failed reading sticks
            System.out.println("Read fail on controllers:  " + e.getMessage());
        }
        return false;
    }

    /**
     *
     * Method to retrieve the state of transmission downshift button. Reads from
     * current active sticks. If using dual joysticks, reads from the right
     * side.
     *
     * @return -- boolean -- true if the button is pressed
     *
     */
    public boolean downShift() {

        // Read and return the value.
        try {
            switch (stickType.value) {
                case Type.STICKS:
                    return rightStick.getRawButton(5);
                case Type.XBOX:
                    return xbox.getRawButton(5);
                case Type.NONE:
                    return false;
            }
        } catch (Exception e) {
            // Something failed reading sticks
            System.out.println("Read fail on controllers:  " + e.getMessage());
        }
        return false;
    }

    /**
     *
     * Method to retrieve the state of tank mode select button. Reads from
     * current active sticks. If using dual joysticks, reads from the right
     * side.
     *
     * @return -- boolean -- true if the button is pressed
     *
     */
    public boolean tankMode() {
        boolean switchvalue = false;

        // Read and return the value.
        try {
            switch (stickType.value) {
                case Type.STICKS:
                    switchvalue = rightStick.getRawButton(8);
                    break;
                case Type.XBOX:
                    switchvalue = xbox.getRawButton(8);
                    break;
                case Type.NONE:
                    switchvalue = false;
            }

        } catch (Exception e) {
            // Something failed reading sticks
            System.out.println("Read fail on controllers:  " + e.getMessage());
        }

        return switchvalue;
    }

    /**
     *
     * Method to retrieve the state of arcade mode select button. Reads from
     * current active sticks. If using dual joysticks, reads from the right
     * side.
     *
     * @return -- boolean -- true if the button is pressed
     *
     */
    public boolean arcadeMode() {
        boolean switchvalue = false;

        // Read and return the value.
        try {
            switch (stickType.value) {
                case Type.STICKS:
                    switchvalue = rightStick.getRawButton(7);
                    break;
                case Type.XBOX:
                    switchvalue = xbox.getRawButton(7);
                    break;
                case Type.NONE:
                    switchvalue = false;
                    break;
            }
        } catch (Exception e) {
            // Something failed reading sticks
            System.out.println("Read fail on controllers:  " + e.getMessage());
        }

        return switchvalue;
    }

    /**
     *
     * Method to retrieve the state of secret weapon deploy button. Reads from
     * current active sticks. If using dual joysticks, reads from the right
     * side.
     *
     * @return -- boolean -- true if the button is pressed
     *
     */
    /*
     Commented out to insure we don't use this as we are using switch on box instead.

     public boolean secretWeaponDeploy() {
        
     // Read and return the value.
     try {
     switch (stickType.value) {
     case Type.STICKS:   
     return rightStick.getRawButton(6);
     case Type.XBOX:
     return xbox.getRawButton(1);
     case Type.NONE:
     return false;
     }        
     }
     catch (Exception e) {
     // Something failed reading sticks
     System.out.println("Read fail on controllers:  " + e.getMessage());
     }
     return false;
     } 
     */
    /**
     *
     * Method to retrieve the state of secret weapon undeploy button. Reads from
     * current active sticks. If using dual joysticks, reads from the right
     * side.
     *
     * @return -- boolean -- true if the button is pressed
     *
     */
    /*  Commented out to insure we don't use this.  Using switch on switch box
     instead.
    
     public boolean secretWeaponUndeploy() {
        
     // Read and return the value.
     try {
     switch (stickType.value) {
     case Type.STICKS:   
     return rightStick.getRawButton(4);
     case Type.XBOX:
     return xbox.getRawButton(2);
     case Type.NONE:
     return false;
     }        
     }
     catch (Exception e) {
     // Something failed reading sticks
     System.out.println("Read fail on controllers:  " + e.getMessage());
     }
     return false;
     } 
    
     */
    /**
     *
     * Method to validate which type of controller to use. This should be run in
     * an iterative loop at each robot cycle to avoid missing switch. Look for
     * which sticks are present and if so is the "activate" button pressed on a
     * stick. If activate is pressed, then switch to that joystick type being
     * current.
     *
     */
    public void checkTypeSwitch() {
        boolean xboxSelect = false;     // Activate xbox controller button pressed
        boolean stickSelect = false;    // Activate joystick button pressed
        boolean noneSelect = true;      // Will only stay true if I couldnt read either type

        // Put exception handler in case xbox is not there
        try {
            if (xbox != null) {
                xboxSelect = xbox.getRawButton(4);
                noneSelect = false; // if we got here we were able to read xbox
            }

        } catch (Exception e) {
            System.out.println("Xbox controller read failed:  " + e.getMessage());
            xboxSelect = false;     // xbox read failed -- make sure we don't use it
        }

        // Put exception handler in case joysticks not there
        try {
            if ((leftStick != null) && (rightStick != null)) {
                stickSelect = (leftStick.getRawButton(11)
                        || rightStick.getRawButton(11));
                noneSelect = false; // if we got here we were able to read both sticks
            }
        } catch (Exception e) {
            System.out.println("Controller read failed:  " + e.getMessage());
            stickSelect = false;    // one or both stick reads failed.  dont use!
        }

        // XBox takes precedence if both select buttons are pushed at same time
        if (xboxSelect) {
            // Set xbox to current joystick type if not already in that mode
            if (stickType != Type.Xbox) {
                stickType = Type.Xbox;
                //System.out.println("Switch to xbox mode");
            }
        } else if (stickSelect) {
            // Set sticks to current joystick type if not already in that mode
            if (stickType != Type.Sticks) {
                stickType = Type.Sticks;
                //System.out.println("Switch to joystick mode");
            }
        } else if (noneSelect) {
            // Neither type of joystick present.  Set mode None if not already.
            if (stickType != Type.None) {
                //System.out.println("Switch to no joystick mode");
                stickType = Type.None;
            }
        }

        // Update SmartDashboard w/ stick data
        // SmartDashboard.putString("Joystick Mode", getTypeString());

    }

    /**
     *
     * Method to "zero" all the joystick axes. Reads each axis and records value
     * as a bias that will be subracted from all future reads. Should only be
     * run when we are sure the joysticks are not being used (i.e. at zero
     * position).
     *
     */
    public void zeroSticks() {
        // Read all the possible stick axes and record as stick bias
        leftXBias = leftStick.getX();
        leftYBias = leftStick.getY();
        leftYawBias = leftStick.getZ();
        rightXBias = rightStick.getX();
        rightYBias = rightStick.getY();
        rightYawBias = rightStick.getZ();
        xLeftXBias = xbox.getRawAxis(1);
        xLeftYBias = xbox.getRawAxis(2);
        xYawBias = xbox.getRawAxis(3);
        xRightXBias = xbox.getRawAxis(4);
        xRightYBias = xbox.getRawAxis(5);

        // Push the bias values onto dashboard
        // SmartDashboard.putNumber("Stick Left X Bias", leftXBias);
        // SmartDashboard.putNumber("Stick Left Y Bias", leftYBias);
        // SmartDashboard.putNumber("Stick Left Yaw Bias", leftYawBias);
        // SmartDashboard.putNumber("Stick Right X Bias", rightXBias);
        // SmartDashboard.putNumber("Stick Right Y Bias", rightYBias);
        // SmartDashboard.putNumber("Stick Right Yaw Bias", rightYawBias);
        // SmartDashboard.putNumber("Xbox Left X Bias", xLeftXBias);
        // SmartDashboard.putNumber("Xbox Left Y Bias", xLeftYBias);
        // SmartDashboard.putNumber("Xbox Right X Bias", xRightXBias);
        // SmartDashboard.putNumber("Xbox Right Y Bias", xRightYBias);
        // SmartDashboard.putNumber("Xbox Yaw Bias", xYawBias);
    }

    /**
     *
     * Method to "unzero" all the joystick axes. Resets all the bias values back
     * to default 0.0 values. This could be used if you no longer want to apply
     * stick zeroing values.
     *
     */
    public void unzeroSticks() {
        // Reset bias values
        leftXBias = 0.0;
        leftYBias = 0.0;
        leftYawBias = 0.0;
        rightXBias = 0.0;
        rightYBias = 0.0;
        rightYawBias = 0.0;
        xLeftXBias = 0.0;
        xLeftYBias = 0.0;
        xYawBias = 0.0;
        xRightXBias = 0.0;
        xRightYBias = 0.0;

        // And push them to dashboard
        // SmartDashboard.putNumber("Stick Left X Bias", leftXBias);
        // SmartDashboard.putNumber("Stick Left Y Bias", leftYBias);
        // SmartDashboard.putNumber("Stick Left Yaw Bias", leftYawBias);
        // SmartDashboard.putNumber("Stick Right X Bias", rightXBias);
        // SmartDashboard.putNumber("Stick Right Y Bias", rightYBias);
        // SmartDashboard.putNumber("Stick Right Yaw Bias", rightYawBias);
        // SmartDashboard.putNumber("Xbox Left X Bias", xLeftXBias);
        // SmartDashboard.putNumber("Xbox Left Y Bias", xLeftYBias);
        // SmartDashboard.putNumber("Xbox Right X Bias", xRightXBias);
        // SmartDashboard.putNumber("Xbox Right Y Bias", xRightYBias);
        // SmartDashboard.putNumber("Xbox Yaw Bias", xYawBias);
    }

        /**
     *
     * Method to apply deadzone handling to stick axis values and to force limit
     * values to proper range of -1.0 to 1.0.
     *
     * @param rawStick -- raw stick input to process
     *
     * @return -- returns the limited and dead zone processed value
     *
     */
    private double dz(double rawStick) {
        final double DZ = 0.10;        // Deadzone is +/- this value
        double stick;

        // Force limit to -1.0 to 1.0
        if (rawStick > 1.0) {
            stick = 1.0;
        } else if (rawStick < -1.0) {
            stick = -1.0;
        } else {
            stick = rawStick;
        }

        // Check if value is inside the dead zone
        if (stick >= 0.0){
            if (Math.abs(stick) >= DZ) 
                return (stick - DZ)/(1 -  DZ);
            else 
                return 0.0;
            
        }
        else {
            if (Math.abs(stick) >= DZ) 
                return (stick + DZ)/(1 - DZ);
            else 
                return 0.0;
            
        }
    }
    public double getThrottle(){
        double value = 0.0;

        // Read and return the value. Negative is "forward"
        try {
            switch (stickType.value) {
                case Type.STICKS:
                    value =  (rightStick.getRawAxis(4));
                    value = (value + 1.0)/ 2.0;
                    break;
                case Type.XBOX:
                    value = 1.0;
                    break;
                case Type.NONE:
                    value = 0.0;
                    break;
            }
        } catch (Exception e) {
            // Something failed reading sticks
            System.out.println("Read fail on controllers:  " + e.getMessage());
        }

        // SmartDashboard.putNumber("Stick RightY", value);
        return value;
        
    }

}
